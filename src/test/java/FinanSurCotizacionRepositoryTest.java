
import com.basededatos14.proveedor.FinanSurCotizacionRepository;
import java.sql.SQLException;
import org.junit.Test;
import static org.junit.Assert.*;

public class FinanSurCotizacionRepositoryTest {

    public FinanSurCotizacionRepositoryTest() {
    }

    @Test(expected = SQLException.class)
    public void obtenerCotizacion_urlInvalida_SQLException() throws SQLException {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUrlLocal("https://api.coindesk.com/v1/bpi/currentprice.xml");
        finanSurCotizacionRepository.obtenerCotizacion();
    }

    @Test
    public void obtenerCotizacion_urlValida_cotizacionObtenida() throws SQLException {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        assertNotNull(finanSurCotizacionRepository.obtenerCotizacion());
    }

    @Test(expected = SQLException.class)
    public void obtenerCotizacion_urlNull_SQLException() throws SQLException {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUrlLocal(null);
        finanSurCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = SQLException.class)
    public void obtenerCotizacion_usuarioInvalido_SQLException() throws SQLException {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setUsuario("otroUser");
        finanSurCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = SQLException.class)
    public void obtenerCotizacion_passwordInvalido_SQLException() throws SQLException {
        FinanSurCotizacionRepository finanSurCotizacionRepository = new FinanSurCotizacionRepository();
        finanSurCotizacionRepository.setPassword("nuevaContr4");
        finanSurCotizacionRepository.obtenerCotizacion();
    }
}
