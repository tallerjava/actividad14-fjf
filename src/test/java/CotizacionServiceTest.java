
//import java.net.ConnectException;
import com.basededatos14.cotizacionservice.CotizacionService;
import com.basededatos14.proveedor.CoinDeskCotizacionRepository;
import com.basededatos14.proveedor.CotizacionRepository;
import java.sql.SQLException;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionServiceTest {

    public CotizacionServiceTest() {
    }

    @Test(expected = NullPointerException.class)
    public void obtenerReporteCotizacion_ArgumentoArrayCotizacionRepositoryNull_NullPointerException() throws SQLException {
        ArrayList<CotizacionRepository> listadoCotizacion = null;
        CotizacionService cotizacionService = new CotizacionService(listadoCotizacion);
        cotizacionService.obtenerReporteCotizacion();
    }

    @Test
    public void obtenerReporteCotizacion_ArgumentoValidoArrayCotizacionRepository_reporteObtenido() throws SQLException {
        ArrayList<CotizacionRepository> listadoCotizacion = new ArrayList();
        listadoCotizacion.add(new CoinDeskCotizacionRepository());
        CotizacionService cotizacionService = new CotizacionService(listadoCotizacion);
        assertNotNull(cotizacionService.obtenerReporteCotizacion());
    }
}
