
import com.basededatos14.cotizacion.Cotizacion;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionTest {

    public CotizacionTest() {
    }

    @Test
    public void getFecha_ValoresValidosObjetoCotizacion_fechaObtenida() {
        String resultadoEsperado = "18-7-2018 17:23";
        Cotizacion cotizacion = new Cotizacion(resultadoEsperado, "USD", 6500.09);
        String resultadoObtenido = cotizacion.getFecha();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void getMoneda_ValoresValidosObjetoCotizacion_monedaObtenida() {
        String resultadoEsperado = "USD";
        Cotizacion cotizacion = new Cotizacion("18-7-2018 17:23", resultadoEsperado, 6500.09);
        String resultadoObtenido = cotizacion.getMoneda();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void getPrecio_ValoresValidosObjetoCotizacion_precioObtenido() {
        double resultadoEsperado = 100.09;
        Cotizacion cotizacion = new Cotizacion("18-7-2018 17:23", "USD", resultadoEsperado);
        double resultadoObtenido = cotizacion.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido, 0.001);
    }

    @Test
    public void toString_ValoresValidosObjetoCotizacion_StringObtenido() {
        String resultadoEsperado = "18-7-2018 17:23 / USD 100,01";
        Cotizacion cotizacion = new Cotizacion("18-7-2018 17:23", "USD", 100.009f);
        String resultadoObtenido = cotizacion.toString();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
}
