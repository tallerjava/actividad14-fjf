package com.basededatos14.reportecotizacion;

import com.basededatos14.cotizacion.Cotizacion;

public class ReporteCotizacion {

    private String nombreProveedor;
    private Cotizacion cotizacion;

    public ReporteCotizacion(String nombreProveedor, Cotizacion cotizacion) {
        this.nombreProveedor = nombreProveedor;
        this.cotizacion = cotizacion;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public Cotizacion getCotizacion() {
        return cotizacion;
    }

    @Override
    public String toString() {
        return nombreProveedor + ": \t" + cotizacion;
    }
}
