package com.basededatos14.proveedor;

import com.basededatos14.cotizacion.Cotizacion;
import java.text.SimpleDateFormat;
import java.util.Date;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class BinanceCotizacionRepository extends CotizacionRepository {

    private String url = "https://api.binance.com/api/v1/ticker/price?symbol=BTCUSDT";
    private String nombreProveedor = "Binance";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    @Override
    public Cotizacion obtenerCotizacion() {
        HttpResponse response = HttpRequest.get(url).send();
        JSONObject body = new JSONObject(response.body());
        String fecha = new SimpleDateFormat("d-M-yyyy HH:mm").format(new Date());
        double precio = body.getDouble("price");
        Cotizacion cotizacion = new Cotizacion(fecha, "USD", precio);
        return cotizacion;
    }
}
