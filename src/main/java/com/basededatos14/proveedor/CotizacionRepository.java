package com.basededatos14.proveedor;

import com.basededatos14.cotizacion.Cotizacion;
import java.sql.SQLException;

public abstract class CotizacionRepository {

    public abstract String getNombreProveedor();

    public abstract Cotizacion obtenerCotizacion() throws SQLException;
}
