package com.basededatos14.proveedor;

import com.basededatos14.cotizacion.Cotizacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FinanSurCotizacionRepository extends CotizacionRepository {

    private String urlLocal = "jdbc:mysql://localhost:3306/finansur";
    private String nombreProveedor = "FinanSur";
    private String usuario = "usuario";
    private String password = "caperusita20";

    public void setUrlLocal(String urlLocal) {
        this.urlLocal = urlLocal;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUrlLocal() {
        return urlLocal;
    }

    public String getUsuario() {
        return usuario;
    }

    @Override
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    @Override
    public Cotizacion obtenerCotizacion() throws SQLException {
        String fecha = null, moneda = null;
        double precio = 0;
        Connection coneccion = DriverManager.getConnection(urlLocal, usuario, password);
        try (Statement estado = coneccion.createStatement()) {
            ResultSet resultado = estado.executeQuery("select * from cotizacion_historial where fecha <= NOW() order by fecha desc limit 0,1;");
            while (resultado.next()) {
                Date fechaDate = resultado.getDate("fecha");
                fecha = new SimpleDateFormat("d-MM-yyyy hh:mm").format(fechaDate);
                moneda = resultado.getString("moneda");
                precio = resultado.getDouble("precio");
            }
            estado.close();
        }
        Cotizacion cotizacion = new Cotizacion(fecha, moneda, precio);
        return cotizacion;
    }
}
