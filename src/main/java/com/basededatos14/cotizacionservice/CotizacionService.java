package com.basededatos14.cotizacionservice;

import com.basededatos14.reportecotizacion.ReporteCotizacion;
import com.basededatos14.cotizacion.Cotizacion;
import com.basededatos14.proveedor.CotizacionRepository;
import java.sql.SQLException;
import java.util.ArrayList;

public class CotizacionService {

    private ArrayList<CotizacionRepository> cotizacionServiceListado;

    public CotizacionService(ArrayList<CotizacionRepository> cotizacionServiceListado) {
        this.cotizacionServiceListado = cotizacionServiceListado;
    }

    public ArrayList<ReporteCotizacion> obtenerReporteCotizacion() throws SQLException {
        ArrayList<ReporteCotizacion> reporteCotizacionListado = new ArrayList();
        for (CotizacionRepository cotizacionRepository : cotizacionServiceListado) {
            String nombreProveedor = cotizacionRepository.getNombreProveedor();
            try {
                Cotizacion cotizacion = cotizacionRepository.obtenerCotizacion();
                reporteCotizacionListado.add(new ReporteCotizacion(nombreProveedor, cotizacion));
            } catch (Exception e) {
                reporteCotizacionListado.add(new ReporteCotizacion(nombreProveedor, new Cotizacion(null, null, -1)));
            }
        }
        return reporteCotizacionListado;
    }
}
