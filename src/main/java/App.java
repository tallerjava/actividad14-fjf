
import com.basededatos14.cotizacionservice.CotizacionService;
import com.basededatos14.reportecotizacion.ReporteCotizacion;
import com.basededatos14.proveedor.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws SQLException {

        System.out.println("Pulse la tecla Enter para obtener la ultima cotizacion del dia");
        Scanner tecla = new Scanner(System.in);
        String enterKey = tecla.nextLine();
        String receivedKey = null;
        do {
            if (enterKey.isEmpty()) {
                ArrayList<CotizacionRepository> cotizacionRepositoryListado = new ArrayList();
                cotizacionRepositoryListado.add(new CoinDeskCotizacionRepository());
                cotizacionRepositoryListado.add(new BinanceCotizacionRepository());
                cotizacionRepositoryListado.add(new SomosPNTCotizacionRepository());
                cotizacionRepositoryListado.add(new CryptoCompareCotizacionRepository());
                cotizacionRepositoryListado.add(new FinanSurCotizacionRepository());
                CotizacionService cotizacionService = new CotizacionService(cotizacionRepositoryListado);
                ArrayList<ReporteCotizacion> reporteCotizacionListado = cotizacionService.obtenerReporteCotizacion();
                for (ReporteCotizacion reporteCotizacion : reporteCotizacionListado) {
                    if (reporteCotizacion.getCotizacion().getPrecio() > -1) {
                        System.out.println(reporteCotizacion);
                    } else {
                        System.out.println(reporteCotizacion.getNombreProveedor() + ":\t(El proveedor no se encuentra disponible)");
                    }
                }
                System.out.println("(Presione Enter para volver a consultar)");
                receivedKey = tecla.nextLine();
            }
        } while (receivedKey != null);

    }
}
